﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="database.aspx.cs" Inherits="Assignment_2a.database" %>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h4>This is what I learned about inner joins</h4>
    <p>Inner joins select reconds that have the same value in both tables</p>
</asp:Content>
<asp:Content ContentPlaceHolderID="Database" runat="server"><br />
   <p><strong>Here's an example of a syntax using inner join to collect the same data from two seperate tables, this is know as bridging tables:</strong></p><br />
    <p>Select bookname, libname, from books inner join booksxlibrarires on books.bookid = books.libraries, = bookid<br />
        inner join libraries on booksxlibraries.libraryid = library.libraryid;</p>
    <strong><p>code that I wrote about inner joins:</p></strong><br />
    <p>select clientfname, clientlname, count (cars.carid) from cars<br />inner join clients on clients.clientid = cars.clients.clientid group by client fname, clientlname;</p>
    <br /> <p>This example collects data from the cars table to compare the amount of cars each client owns.</p>
    <a href="https://www.w3schools.com/sql/sql_join_inner.asp">Learn more here</a>
</asp:Content>

